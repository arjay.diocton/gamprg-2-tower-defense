// Fill out your copyright notice in the Description page of Project Settings.

#include "Enemy.h"
#include "EnemyAIController.h"

void AEnemyAIController::OnMoveCompleted(FAIRequestID RequestID, const FPathFollowingResult& Result)
{
	AEnemy* enemy = Cast<AEnemy>(GetPawn()); // or get controlled pawn if this doesn't work

	if (enemy) {
		enemy->wayPointCurrentIndex++;
		enemy->MoveToWayPoint();

	}
}
