// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "GameFramework/FloatingPawnMovement.h"
#include "AIController.h"
#include "Enemy.generated.h"

UCLASS()
class MYPROJECT_API AEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AEnemy();

	//UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	//	class UFloatingPawnMovement* movementComponent;

	int32 moveSpeed;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		class UStaticMeshComponent* meshComponentStatic;


	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
		int32 wayPointCurrentIndex = 0;

	//UPROPERTY(EditAnywhere, BlueprintReadWrite)

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	TArray<class AActor* > wayPoints;

private:
	

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		void OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void MoveToWayPoint();

	// Called to bind functionality to input
	//virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

};
