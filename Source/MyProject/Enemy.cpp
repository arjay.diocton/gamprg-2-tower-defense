// Fill out your copyright notice in the Description page of Project Settings.


#include "EnemyAIController.h"
#include "WayPoint.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy.h"

// Sets default values
AEnemy::AEnemy()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//RootComponent = CreateDefaultSubobject<USceneComponent>("Scene");

	//movementComponent = CreateDefaultSubobject<UFloatingPawnMovement>("MovementCompenent");

	meshComponentStatic = CreateDefaultSubobject<UStaticMeshComponent>("Static Mesh");

	meshComponentStatic->SetupAttachment(RootComponent);
	

}

// Called when the game starts or when spawned
void AEnemy::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AWayPoint::StaticClass(), wayPoints);
	MoveToWayPoint();

}

void AEnemy::OnComponentHit(UPrimitiveComponent* HitComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
}

// Called every frame
void AEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


}

void AEnemy::MoveToWayPoint()
{
	AEnemyAIController* enemyAIController = Cast<AEnemyAIController>(GetController());
	GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("Function is entering"), wayPointCurrentIndex));

	if (enemyAIController) 
	{
		if (wayPointCurrentIndex <= wayPoints.Num())
		{

			for (AActor* Waypoint : wayPoints) 
			{
				AWayPoint* waypointRef = Cast<AWayPoint>(Waypoint);

				if (waypointRef) 
				{
					if (waypointRef->wayPointOrderGetter() == wayPointCurrentIndex)
					{
						enemyAIController->MoveToActor(waypointRef, 5.f, false);
						GEngine->AddOnScreenDebugMessage(-1, 15.0f, FColor::Yellow, FString::Printf(TEXT("World delta for current frame equals %f"), wayPointCurrentIndex));

						break;
					}
				}
			
			}
		}
	}
}

// Called to bind functionality to input
//void AEnemy::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
//{
//	Super::SetupPlayerInputComponent(PlayerInputComponent);
//
//}

